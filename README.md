# birthday-telegram-bot



Birthday Telegram Bot provides next functions:
- show the nearest birthday
- show all birthdays
- notify when the next birthday will be after 2 weeks
- send celebration text

## List of commands
```
- /whonext - Show the nearest birthday
- /showall - Show all birthdays
- /celebrate - Send celebration text if today is birthday
```
## Configuration
### Setup
1. Create your telegram bot using [BotFather](https://t.me/botfather)
2. Clone project
3. Execute ```composer install``` command in project root directory
4. Create .env.local in root directory and provide values for variables:
    ```
    DATABASE_URL=<YOUR_DATABASE_CONNECTION_STRING>
    API_TOKEN=YOUR_TELEGRAM_BOT_KEY
    TG_GROUP_ID=YOUR_GROUP_CHAT_ID
    ```
5. Create database and tables:
    ```
    bin/console doctrine:database:create
    bin/console doctrine:migrations:migrate
    ```
6. Start the server:
    ```
    symfony server:start
    ```
7. For using bot locally, [install ngrok](https://ngrok.com/download) for creating tunnel and execute next command:
    ```
    ./ngrok http YOUR_HOST
    ```
    After executing ngrok generates domain names (http/https).
8. Set webhook for telegram in browser. Use your bot key and ngrok https domain name for url parameter:
    ```
    https://api.telegram.org/bot<YOUR_BOT_ID>/setWebhook?url=NGROK_HTTPS_HOST
    ```
### Configure CRON tasks
Birthday Telegram Bot provides command for notifying about birthday 2 weeks before and sending celebrate message in group chat.
```
php bin/console birthday:celebrate
```
Guides for CRON configuration:
- [For Windows](https://active-directory-wp.com/docs/Usage/How_to_add_a_cron_job_on_Windows/Scheduled_tasks_and_cron_jobs_on_Windows/index.html)
- [For Linux](https://www.cyberciti.biz/faq/how-do-i-add-jobs-to-cron-under-linux-or-unix-oses/)
