<?php

namespace App\Command;

use App\Service\BirthdayService;
use App\Service\FormatService;
use App\Service\TelegramService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BirthdayNotifierCommand extends Command
{
    protected static $defaultName = 'birthday:celebrate';
    protected static $defaultDescription = 'Add a short description for your command';
    private BirthdayService $birthdayService;
    private FormatService $formatService;
    private TelegramService $telegramService;
    private string $groupChatId;

    public function __construct(
        string $name = null,
        $groupChatId,
        BirthdayService $birthdayService,
        FormatService $formatService,
        TelegramService $telegramService
    )
    {
        parent::__construct($name);
        $this->birthdayService = $birthdayService;
        $this->formatService = $formatService;
        $this->telegramService = $telegramService;
        $this->groupChatId = $groupChatId;
    }

    protected function configure(): void {}

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $currentDayOfYear = (int) date('z');
        $persons = $this->birthdayService->whoIsNext();

        $personsBirthday = $this->formatService->formatBirthdayDate($persons[0]->getBirthDate());

        if (($personsBirthday - $currentDayOfYear === 14) && !empty($this->groupChatId)) {
            $this->telegramService->sendMessage($this->groupChatId, $this->birthdayService->sendWhoIsNext());
        }

        if ($this->birthdayService->checkTodayIsBirthday()) {
            $this->telegramService->sendMessage($this->groupChatId, $this->birthdayService->sendCelebrationText());
        }

        return Command::SUCCESS;
    }
}
