<?php

namespace App\Controller;

use App\Service\BirthdayService;
use App\Service\BotTimeoutService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\TelegramService;


class BotController extends AbstractController
{
    private BirthdayService $birthdayService;
    private BotTimeoutService $botTimeoutService;
    private TelegramService $telegramService;

    public function __construct(
        BirthdayService $birthdayService,
        BotTimeoutService $botTimeoutService,
        TelegramService $telegramService
    )
    {
        $this->birthdayService = $birthdayService;
        $this->botTimeoutService = $botTimeoutService;
        $this->telegramService = $telegramService;
    }

    /**
     * @Route("/", name="bot")
     * @return Response
     */
    public function index(): Response
    {
        $result = $this->telegramService->getUpdates();
        $data = [];

        $data['command_name'] = !empty($result["message"]["text"]) ? str_replace($this->telegramService->getBotName(), '', $result["message"]["text"]) : '';
        $data['chat_id'] = !empty($result["message"]) ? $result["message"]["chat"]["id"] : '';
        $data['date'] = !empty($result["message"]) ? $result["message"]["date"] : '';

        if (!$this->botTimeoutService->checkIfCommandExists($data)) {
            if ($this->botTimeoutService->isInformed($data)) {
                return new Response();
            }
            $this->telegramService->sendImg($data['chat_id']);
            return new Response();
        }

        switch ($data['command_name']) {
            case '/whonext':
                $this->botTimeoutService->saveCommand($data);
                $this->telegramService->sendMessage($data['chat_id'], $this->birthdayService->sendWhoIsNext());
                break;
            case '/showall':
                $this->botTimeoutService->saveCommand($data);
                $this->telegramService->sendMessage($data['chat_id'], $this->birthdayService->sendAllBirthdays());
                break;
            case '/celebrate':
                $this->botTimeoutService->saveCommand($data);
                if ($this->birthdayService->checkTodayIsBirthday()) {
                    $this->telegramService->sendMessage($data['chat_id'], $this->birthdayService->sendCelebrationText());
                }
                break;
        }

        return new Response();
    }
}
