<?php

namespace App\Repository;

use App\Entity\ExecutedCommand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExecutedCommand|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExecutedCommand|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExecutedCommand[]    findAll()
 * @method ExecutedCommand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExecutedCommandRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExecutedCommand::class);
    }

    public function findLatestCommand($chatId, $commandName): array
    {
        return $this->createQueryBuilder('e')
            ->select('MAX(e.date) AS date')
            ->where('e.chat_id = :chat_id')
            ->andWhere('e.command_name = :command_name')
            ->setParameter('chat_id', $chatId)
            ->setParameter('command_name', $commandName)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findCommand($chatId, $commandName, $date)
    {
        return $this->createQueryBuilder('e')
            ->where('e.chat_id = :chat_id')
            ->andWhere('e.command_name = :command_name')
            ->andWhere('e.date = :date')
            ->setParameter('chat_id', $chatId)
            ->setParameter('command_name', $commandName)
            ->setParameter('date', $date)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
