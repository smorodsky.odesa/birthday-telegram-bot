<?php

namespace App\Service;

use App\Repository\BirthdayRepository;

class BirthdayService
{
    private FormatService $formatService;
    private BirthdayRepository $birthdayRepository;

    public function __construct(FormatService $formatService, BirthdayRepository $birthdayRepository)
    {
        $this->formatService = $formatService;
        $this->birthdayRepository = $birthdayRepository;
    }

    public function whoIsNext(): array
    {
        $theNearestBirthDay = 0;
        $currentDayOfYear = date('z');
        $nextBirthdays = [];

        $birthdayPeople = $this->birthdayRepository->findAll();

        foreach ($birthdayPeople as $person) {
            $personBirthDayInYear = $this->formatService->formatBirthdayDate($person->getBirthDate());
            if ((int) $currentDayOfYear > $personBirthDayInYear) {
                continue;
            }
            if (empty($theNearestBirthDay) || $theNearestBirthDay > $personBirthDayInYear) {
                $nextBirthdays = [];
                $theNearestBirthDay = $personBirthDayInYear;
                $nextBirthdays[] = $person;
            } elseif ($theNearestBirthDay === $personBirthDayInYear) {
                $nextBirthdays[] = $person;
            }
        }

        return $nextBirthdays;
    }

    public function showAllBirthdays(): array
    {
        $birthdayPeople = $this->birthdayRepository->findAll();

        usort($birthdayPeople, function ($personA, $personB) {
           $personADayNumber = $this->formatService->formatBirthdayDate($personA->getBirthDate());
           $personBDayNumber = $this->formatService->formatBirthdayDate($personB->getBirthDate());
           if ($personADayNumber === $personBDayNumber) {
               return 0;
           }
           return ($personADayNumber < $personBDayNumber)  ? -1 : 1;
        });

        return $birthdayPeople;
    }

    public function sendWhoIsNext(): string
    {
        return $this->formatService->formatMessage(
            $this->whoIsNext()
        );
    }

    public function sendAllBirthdays(): string
    {
        return $this->formatService->formatMessage(
            $this->showAllBirthdays()
        );
    }

    public function sendCelebrationText(): string
    {
        return $this->formatService->formatCelebrationMessage(
            $this->whoIsNext()
        );
    }

    public function checkTodayIsBirthday()
    {
        $currentDayOfYear = (int) date('z');
        $persons = $this->whoIsNext();

        $personsBirthday = $this->formatService->formatBirthdayDate($persons[0]->getBirthDate());

        if ($personsBirthday - $currentDayOfYear === 0) {
            return true;
        }

        return false;
    }
}
