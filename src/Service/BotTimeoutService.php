<?php

namespace App\Service;

use App\Entity\ExecutedCommand;
use App\Repository\ExecutedCommandRepository;
use Doctrine\ORM\EntityManager;

class BotTimeoutService
{
    private EntityManager $entityManager;
    private ExecutedCommandRepository $executedCommandRepository;

    public function __construct($entityManager,  ExecutedCommandRepository $executedCommandRepository)
    {
        $this->entityManager = $entityManager;
        $this->executedCommandRepository = $executedCommandRepository;
    }

    public function checkIfCommandExists(array $commandData): bool
    {
        $latestCommand = $this->executedCommandRepository->findLatestCommand($commandData['chat_id'], $commandData['command_name']);

        if (!empty($latestCommand)) {
            $currentTimestamp = time();
            $commandTimestamp = strtotime($latestCommand['date']);
            if ($currentTimestamp - $commandTimestamp < 300) {
                return false;
            }
        }

        return true;
    }

    public function saveCommand(array $commandData): void
    {
        $command = new ExecutedCommand();
        $datetime = new \DateTime();
        $command->setCommandName($commandData['command_name'])
            ->setDate($datetime->setTimestamp($commandData['date']))
            ->setChatId($commandData['chat_id']);
        $this->entityManager->persist($command);
        $this->entityManager->flush();
    }

    public function isInformed(array $commandData): bool
    {
        $latestCommand = $this->executedCommandRepository->findLatestCommand($commandData['chat_id'], $commandData['command_name']);

        $result = $this->executedCommandRepository->findCommand(
            $commandData['chat_id'],
            $commandData['command_name'],
            $latestCommand['date']
        );
        if (empty($result)) {
            return false;
        }
        if (empty($result->getInformed())) {
            $command = $this->executedCommandRepository->find($result->getId());
            $command->setInformed(true);
            $this->entityManager->flush();

            return false;
        }

        return true;
    }
}
