<?php

namespace App\Service\Celebration;

use App\Service\Celebration\Parser\ParserInterface;
use App\Service\Celebration\Source\SourceInterface;

class BirthdayCelebrationService implements CelebrateInterface
{
    private ParserInterface $parserInterface;
    private SourceInterface $sourceInterface;

    public function __construct(SourceInterface $sourceInterface, ParserInterface $parserInterface)
    {
        $this->parserInterface = $parserInterface;
        $this->sourceInterface = $sourceInterface;
    }

    public function getCelebrationText(): string
    {
        return $this->parserInterface->parseContent($this->sourceInterface->getContent());
    }
}
