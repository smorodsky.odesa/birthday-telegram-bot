<?php

namespace App\Service\Celebration;

interface CelebrateInterface
{
    public function getCelebrationText(): string;
}
