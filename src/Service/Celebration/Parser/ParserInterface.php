<?php

namespace App\Service\Celebration\Parser;

interface ParserInterface
{
    public function parseContent(string $content): string;
}
