<?php

namespace App\Service\Celebration\Parser;

class PozdravuhaParser implements ParserInterface
{
    private const TEXT_SELECTOR = '#<div class="pozdravuha_ru_text">(.*?)<\/div>#';

    public function parseContent(string $content): string
    {
        preg_match_all(self::TEXT_SELECTOR, $content, $matches);

        return str_replace('<br>', "\n", $matches[1][$this->getCelebrationTextIndex(count($matches[1]) - 1)]);
    }

    private function getCelebrationTextIndex(int $count): int
    {
        return rand(0, $count);
    }
}
