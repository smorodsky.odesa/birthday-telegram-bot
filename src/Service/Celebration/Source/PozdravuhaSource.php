<?php

namespace App\Service\Celebration\Source;

use GuzzleHttp\Client;

class PozdravuhaSource implements SourceInterface
{
    private const SOURCE_URL = 'https://www.pozdravuha.ru/p/s-dnem-rojdenia-pozhelaniya/pozdrav/';
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    private function generateUrl(): string
    {
        $pageNumber = rand(1, 25);
        return self::SOURCE_URL . $pageNumber;
    }

    public function getContent(): string
    {
        return mb_convert_encoding($this->client->request('GET', $this->generateUrl())->getBody(), "utf-8", "Windows-1251");
    }
}
