<?php

namespace App\Service\Celebration\Source;

interface SourceInterface
{
    public function getContent(): string;
}
