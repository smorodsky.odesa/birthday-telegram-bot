<?php

namespace App\Service;

use App\Entity\Birthday;
use App\Service\Celebration\BirthdayCelebrationService;
use App\Service\Celebration\Parser\PozdravuhaParser;
use App\Service\Celebration\Source\PozdravuhaSource;
use GuzzleHttp\Client;

class FormatService
{
    public const EMOJI_PARTY = "\xF0\x9F\xA5\xB3";
    public const EMOJI_GRINNING_FACE = "\xF0\x9F\x98\x81";
    public const EMOJI_POPPING_CORK = "\xF0\x9F\x8D\xBE";

    public function formatBirthdayDate(\DateTime $date): int
    {
        $personBD = explode('-', $date->format('m-d'));

        return (int) date('z', mktime(0, 0, 0, $personBD[0], $personBD[1], date('Y')));
    }

    /**
     * @param Birthday[] $persons
     * @return string
     */
    public function formatMessage(array $persons): string
    {
        $message = 'Birthdays '. self::EMOJI_GRINNING_FACE . self::EMOJI_POPPING_CORK . self::EMOJI_POPPING_CORK . "\n";
        foreach ($persons as $person) {
            $message .= self::EMOJI_PARTY . "<b>{$person->getPersonName()}</b>: {$person->getBirthDate()->format('F j')} \n";
        }

        return $message;
    }

    public function formatCelebrationMessage(array $persons): string
    {
        $message = '';
        $birthdayCelebrationService = new BirthdayCelebrationService(new PozdravuhaSource(new Client()), new PozdravuhaParser());
        foreach ($persons as $person) {
            $personNickname = $person->getNickname() ?? $person->getPersonName();
            $celebrationText = $birthdayCelebrationService->getCelebrationText();
            $message .= "{$personNickname}, с Днем Рождения! " . self::EMOJI_PARTY . " \n\n";
            $message .= "{$celebrationText} \n\n";
        }

        return $message;
    }
}
