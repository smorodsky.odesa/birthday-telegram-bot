<?php

namespace App\Service;

use Telegram\Bot\Api;

class TelegramService
{
    private Api $botApi;
    private string $waitVideo;

    public function __construct($apiToken, $waitVideo)
    {
        $this->botApi = new Api($apiToken);
        $this->waitVideo = $waitVideo;
    }

    public function getBotName(): string
    {
        return '@' . $this->botApi->getMe()->getUsername();
    }

    public function getUpdates()
    {
        return $this->botApi->getWebhookUpdates();
    }

    public function sendMessage($chatId, $message)
    {
        $this->botApi->sendMessage(['chat_id' => $chatId, 'text' => $message, 'parse_mode' => 'html']);
    }

    public function sendImg($chatId)
    {
        $this->botApi->sendVideo(['chat_id' => $chatId, 'video' => $this->waitVideo]);
    }
}
